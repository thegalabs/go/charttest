module gitlab.com/thegalabs/go/charttest

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	helm.sh/helm/v3 v3.6.3
	rsc.io/letsencrypt v0.0.3 // indirect
)
