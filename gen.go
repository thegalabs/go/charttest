package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/stretchr/testify/assert"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/cli/values"
	"helm.sh/helm/v3/pkg/getter"
)

const expSuffix = "-expected.yml"
const valSuffix = "-values.yml"
const genSuffix = "-generated.yml"

// a testify testingT implementation that just prints to console
type notTestingT struct{}

func (notTestingT) Errorf(format string, args ...interface{}) {
	log.Printf(format, args...)
}

func expectedPath(dir, prefix string) string {
	return path.Join(dir, prefix+expSuffix)
}

func valuesPath(dir, prefix string) string {
	return path.Join(dir, prefix+valSuffix)
}

func generatedPath(dir, prefix string) string {
	return path.Join(dir, prefix+genSuffix)
}

func allPrefixes(dir string) ([]string, error) {
	files, err := filepath.Glob(path.Join(dir, "*"+valSuffix))
	if err != nil {
		return nil, err
	}

	prefixes := make([]string, len(files))
	for i := range files {
		prefixes[i] = strings.TrimSuffix(path.Base(files[i]), valSuffix)
	}

	for _, p := range prefixes {
		expPath := expectedPath(dir, p)
		if _, err = os.Stat(expPath); os.IsNotExist(err) {
			return nil, fmt.Errorf("expected file not found at %s", expPath)
		}
	}
	return prefixes, nil
}

func generate(ch *chart.Chart, valuesPath string) (string, error) {
	settings := cli.New()
	cfg := new(action.Configuration)
	if err := cfg.Init(settings.RESTClientGetter(), "", "", log.Printf); err != nil {
		return "", err
	}

	client := action.NewInstall(cfg)
	client.DryRun = true
	client.Replace = true // Skip the name check
	client.ClientOnly = true
	client.ReleaseName = "Test Release"

	opts := new(values.Options)
	opts.ValueFiles = []string{valuesPath}

	p := getter.All(settings)
	vals, err := opts.MergeValues(p)
	if err != nil {
		return "", err
	}

	r, err := client.Run(ch, vals)
	if err != nil {
		return "", err
	}
	return r.Manifest, nil
}

func main() {
	var relTestDir string
	var gen bool
	flag.StringVar(&relTestDir, "t", "./tests", "the path to the test dir, relative to the chart directory")
	flag.BoolVar(&gen, "g", false, "if true the files are generated")
	flag.Parse()

	root := flag.Arg(0)
	if len(root) == 0 {
		log.Printf("Usage: %s [OPTIONS] path-to-chart\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	wd, err := os.Getwd()
	if err != nil {
		log.Fatalf("could not get wd with %+v", err)
	}
	root = filepath.Join(wd, root)

	testDir := filepath.Join(root, relTestDir)
	prefixes, err := allPrefixes(testDir)
	if err != nil {
		log.Fatalf("could not load files in dir %s with err: %+v", testDir, err)
	} else if len(prefixes) == 0 {
		log.Fatalf("could not find files in dir %s", testDir)
	}

	ch, err := loader.Load(root)
	if err != nil {
		log.Fatalf("could not load chart at path %s with err: %+v", root, err)
	}

	for _, pf := range prefixes {
		out, err := generate(ch, valuesPath(testDir, pf))
		if err != nil {
			log.Fatalf("failed generating %s with %+v", pf, err)
		}

		if gen {
			outPath := generatedPath(testDir, pf)
			if err = ioutil.WriteFile(outPath, []byte(out), 0600); err != nil {
				log.Fatalf("could not write file at %s with %+v", outPath, err)
			} else {
				log.Printf("Generated file at %s", outPath)
			}
		} else {
			expPath := expectedPath(testDir, pf)
			exp, err := ioutil.ReadFile(expPath)
			if err != nil {
				log.Fatalf("could not read expected at %s with %+v", expPath, err)
			}

			if !assert.Equal(notTestingT{}, string(exp), out) {
				log.Fatalf("Files with prefix %s did not match", pf)
			} else {
				log.Printf("Done checking file with prefix %s", pf)
			}
		}
	}
}
